// styles
import './ImgBtn.scss'

export default function ImgBtn({ img, imgStyle, onClick, ...props}) {
  return (
    <button 
      className="img-btn flex-center" 
      onClick={onClick}
      {...props}
    >
      <img 
        src={img} 
        style={imgStyle}
      />
    </button>
  )
}
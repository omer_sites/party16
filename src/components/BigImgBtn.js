// styles
import './BigImgBtn.scss'

export default function BigImgBtn({ children, disabled, img, imgStyle, tool, ...props }) {
  return !disabled ? (
    <button 
      className='big-button flex-center' 
      {...props}
    >
      <img src={img} style={imgStyle} />
      {tool && <div className="tool flex-center">{tool}</div>}
      {children}
    </button>
  ) : (
  <div 
    className='big-button flex-center' 
    {...props}
  >
    <img src={img} style={imgStyle} />
    {tool && <div className="tool flex-center">{tool}</div>}
    {children}
  </div>
  )
}
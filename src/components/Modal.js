// containers
import FadeIn from '../containers/FadeIn'
import PopIn from '../containers/PopIn'

// containers
import DelayRender from '../containers/DelayRender'

// styles
import './Modal.scss'

export default function Modal({ children, showModal, setShowModal, ...props }) {
  return (
    <FadeIn inState={showModal}>
      <div className="modal flex-center" onClick={() => setShowModal(false)} >
        <DelayRender>
          <PopIn inState={showModal}>
            <div className="modal-window flex-center" {...props} onClick={e => e.stopPropagation()}>
              {children}
            </div>
          </PopIn>
        </DelayRender>
      </div>
    </FadeIn>
  )
}
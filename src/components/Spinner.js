import { SpinnerDotted } from "spinners-react"

export default function Spinner(props) {
  return (
    <SpinnerDotted size={150} thickness={150} speed={100} color="rgba(243, 100, 100, 1)" style={{ 
      maxWidth: '100%', 
      margin: '1em'
    }}{...props}/>
  )
}
import { useState } from "react"

// components
import Input from "./Input"
import ImgBtn from "./ImgBtn"
import ArrowForward from '../assets/arrow_forward.svg'

// styles
import './SingleInputForm.scss'

export default function Login_form({ onSubmit, placeholder }) {

  const state = useState('')

  const handleSubmit = e => {
    e.preventDefault()
    if (state[0]) {
      onSubmit(state[0])
    } else {
      alert('יש להכניס טלפון!')
    }
  }

  return (
    <form className="login-form" onSubmit={handleSubmit}>
      <Input type="number" state={state} placeholder={placeholder} />
      <ImgBtn img={ArrowForward}/>
    </form>
  )
}
const inputStyle = {
  fontFamily: 'inherit',
  margin: '.2em 0',
  padding: '.2em .5em',
  border: 'none',
  borderRadius: '.5em',
  boxShadow: '0 0 .5em #ddd'
}

export default function Input({ state, ...props }) {

  const [currState, setState] = state

  return (
    <input 
      className="input"
      value={currState}
      onChange={e => setState(e.target.value)}
      style={inputStyle}
      {...props}
    />
  )
}
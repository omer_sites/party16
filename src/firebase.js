import { initializeApp } from "firebase/app"
import { getFirestore } from "firebase/firestore"
import { getAuth } from "firebase/auth"

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDywqLvs5QwwR9dz3yVk-vcrOli7CWH9Qc",
  authDomain: "party-16.firebaseapp.com",
  projectId: "party-16",
  storageBucket: "party-16.appspot.com",
  messagingSenderId: "360080909614",
  appId: "1:360080909614:web:0c1f6991827eb0cc47c6bf"
}

// Initialize Firebase
const app = initializeApp(firebaseConfig)
const db = getFirestore()
const auth = getAuth()
auth.languageCode = 'iw'

export { app, db, auth }
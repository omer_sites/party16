import { useRef } from 'react'
import styled from 'styled-components'

// CSSTransition
import { CSSTransition } from 'react-transition-group'


const TransitionDiv = styled.div`
  z-index: 999;
  opacity: 0;
  pointer-events: none;
  transition: opacity .2s;

  &.fade-in-appear-active, &.fade-in-appear-done {
    pointer-events: all;
    opacity: 1;
  }
  
  &.fade-in-enter-active, &.fade-in-enter-done {
    pointer-events: all;
    opacity: 1;
  }
`

export default function FadeIn({ children, inState = true, ...props }) {

  const nodeRef = useRef(null)

  return (
    <CSSTransition
      nodeRef={nodeRef} 
      in={inState}
      appear={true} 
      timeout={200}
      classNames="fade-in"
    >
      <TransitionDiv ref={nodeRef} {...props}>
        {children}
      </TransitionDiv>
    </CSSTransition>
  )
}

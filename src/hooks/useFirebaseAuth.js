import { auth } from '../firebase'
import { RecaptchaVerifier, signInWithPhoneNumber } from "firebase/auth";

export default function useFirebaseAuth() {

  const UseRecaptcha = (btnId, renderCallback, verifyCallback) => {
    window.recaptchaVerifier = new RecaptchaVerifier(btnId, {
      'size': 'invisible',
      'callback': () => {
        verifyCallback();
      }
    }, auth)
    window.recaptchaVerifier.render().then(() => {
      renderCallback()
    })
  }

  const LoginWithPhone = async phone => {
    phone = formatPhoneIL(phone)
    const appVerifier = window.recaptchaVerifier
    try {
      const confirmationResult = await signInWithPhoneNumber(auth, phone, appVerifier)
      window.confirmationResult = confirmationResult
      return confirmationResult

      // SMS sent. Prompt user to type the code from the message, then sign the
      // user in with confirmationResult.confirm(code).

    } catch (err) {
      throw new Error(err)
    }
  }

  return { UseRecaptcha, LoginWithPhone }
}

const formatPhoneIL = phone => {
  return '+972'+phone.substring(1)
}
import { useContext } from "react"
import { UserContext } from "../containers/UserProvider"

export default function useUser() {

  const context = useContext(UserContext)

  const findUserInList = (user, list) => {
    const userData = list.find(doc => doc.phone == formatPhone(user.phoneNumber));
    return userData
  }

  return { user: context.user, findUserInList }
}

const formatPhone = phone => {
  return '0'+phone.substring(4)
}

import { useState, useEffect } from 'react'

// custom hooks
import useUser from '../hooks/useUser'

// components
import QRButton_paid from './QRButton_paid'
import QRButton_unpaid from './QRButton_unpaid'

export default function QRButton({ list, user }) {

  const { findUserInList } = useUser()
  const [userData, setUserData] = useState({})
  const [paid, setPaid] = useState(false)

  useEffect(() => {
    setUserData(findUserInList(user, list))
  }, [list, user])

  useEffect(() => {
    userData.paid ? setPaid(true) : setPaid(false)
  }, [userData])

  return (
    paid ? (
      <QRButton_paid userData={userData} />
    ) : (
      <QRButton_unpaid />
    )
  )
}
import { useEffect, useState } from 'react'
import styled from 'styled-components'

// custom hooks
import useFirestoreCollection from '../hooks/useFirestoreCollection'
import useFirestore from '../hooks/useFirestore'

// components
import AddInvitee from '../components/AddInvitee'
import List from '../components/List'

// styles
const InvitationListDiv = styled.div`
  position: relative;
  width: fit-content;
  max-width: 100%;
  padding: 0 1em;
`

export default function InvitationList({ list, user }) {

  const [isAdmin, setIsAdmin] = useState(false)
  const { addDocument, removeDocument, updateDocument } = useFirestore('invitation_list')
  const { documents: admins } = useFirestoreCollection('admin_list')
  useEffect(() => {
    if (admins.some(obj => formatPhoneIL(obj.phone) === user.phoneNumber)) {
      setIsAdmin(true)
    } else {
      setIsAdmin(false)
    }
  }, [admins, user])

  const handleXClick = id => {
    window.confirm('למחוק מוזמן/ת?') 
      && removeDocument(id)
  }

  const handlePaid = (id, isPaid) => {
    updateDocument(id, {
      paid: isPaid
    })
  }

  return (
    <InvitationListDiv className="flex-column">
      {isAdmin && <AddInvitee addDocument={addDocument}/>}
      <List listArr={list} isAdmin={isAdmin} onXClick={handleXClick} onPaid={handlePaid} />
    </InvitationListDiv>
  )
}

const formatPhoneIL = phone => {
  return '+972'+phone.substring(1)
}

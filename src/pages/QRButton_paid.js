import { useState, useEffect } from 'react'
import QRCode from "qrcode.react"

// components
import BigImgBtn from '../components/BigImgBtn'
import Modal from '../components/Modal'

// images
import QRImg from '../assets/qr.svg'

export default function QRButton_paid({ userData }) {

  const [showModal, setShowModal] = useState(false)
  const [QRValue, setQRValue] = useState('')

  useEffect(() => {
    if (userData) {
      setQRValue(`
        שם: ${userData.name}
        טלפון: ${addDashes(userData.phone)}
        סטטוס תשלום: ${userData.paid ? 'שולם' : 'לא שולם'}
      `)
    }
  }, [userData])

  const handleClick = () => {
    setShowModal(true)
  }
  
  return (
    <>
      <BigImgBtn
        onClick={handleClick}
        img={QRImg}
        tool-bottom="לברקוד אישור תשלום"
        style={{ '--color': '#b8e5ff', zIndex: '1'}}
      />
      <Modal 
        showModal={showModal} 
        setShowModal={setShowModal}
        style={{background: '#eaffee', padding: '2em'}}
      >
        <QRCode value={QRValue} size={250} style={{ height: 'unset' }} />
      </Modal>
    </>
  )
}

const addDashes = phone => {
  const phone_val = phone.replace(/\D[^\.]/g, "");
  return phone_val.slice(0,3)+"-"+phone_val.slice(3,6)+"-"+phone_val.slice(6);
}
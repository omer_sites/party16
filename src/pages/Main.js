import styled from 'styled-components'

// custom hooks
import useUser from '../hooks/useUser'

// components
import Main_login from './Main_login';
import Main_loggedIn from './Main_loggedIn';
import DelayRender from '../containers/DelayRender'

// styles
const MainDiv = styled.div`
  width: 100%;
  padding: 1em;

  a {
    width: 10em;
    height: 10em;
    margin: 1em;
    padding: 1em;
    border-radius: 50%;
    background: red;
  }
`

export default function Main() {

  const { user } = useUser()
  
  return (
    <MainDiv className='flex-column'>
      {user ? (
        <Main_loggedIn user={user} />
      ) : (
        <DelayRender>
          <Main_login />
        </DelayRender>
      )}
    </MainDiv>
  )
}
import { useState, useEffect } from "react"

// custom hooks
import useFirestoreCollection from "../hooks/useFirestoreCollection"

// containers
import PopIn from '../containers/PopIn'

// components
import ConfirmButton from "./ConfirmButton"
import QRButton from "./QRButton"
import InvitationList from "./InvitationList"
import Main_loggedIn_uninvited from "./Main_loggedIn_uninvited"
import Spinner from  "../components/Spinner"

export default function Main_loggedIn({ user }) {
  
  const { documents: list, isPending } = useFirestoreCollection('invitation_list')
  const [isInvited, setIsInvited] = useState(false)

  useEffect(() => {    
    if (list.some(e => formatPhoneIL(e.phone) === user.phoneNumber)) {
      setIsInvited(true)
    } else {
      setIsInvited(false)
    }
  }, [list, user])

  return (
    !isPending ? (
      isInvited ? (
        <>
        <PopIn className="flex-column">
          <ConfirmButton list={list} user={user} />
          <QRButton list={list} user={user} />
          <InvitationList list={list} user={user} />
        </PopIn>
        </>
      ) : (
        <PopIn className="flex-column">
          <Main_loggedIn_uninvited />
        </PopIn>
      )
    ): <Spinner />
  )
}

const formatPhoneIL = phone => {
  return '+972'+phone.substring(1)
}
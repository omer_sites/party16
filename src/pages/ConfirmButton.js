import { useState, useEffect } from 'react'

// custom hooks
import useFirestore from '../hooks/useFirestore'

// containers
import PopIn from '../containers/PopIn'

// components
import ConfirmButton_confirm from './ConfirmButton_confirm'
import ConfirmButton_confirmed from './ConfirmButton_confirmed'
import Spinner from '../components/Spinner'

export default function ConfirmButton({ list, user }) {

  const { findDocument, updateDocument } = useFirestore('invitation_list')
  const [arriving, setArriving] = useState(false)
  const [fetching, setFetching] = useState(false)

  useEffect(() => {
    const userData = list.find(doc => doc.phone == formatPhone(user.phoneNumber));
    if (userData.arriving) {
      setArriving(true)
    } else {
      setArriving(false)
    }
  }, [list, user])
  

  const handleClick = async (isArriving) => {
    setFetching(true)
    const query = await findDocument(['phone', '==', formatPhone(user.phoneNumber)])
    query.docs.forEach(doc => {
      updateDocument(doc.data().id, {
        arriving: isArriving
      }).then(() => {
        setFetching(false)
      })
    })
  }

  return !fetching ? (
    !arriving ? (
      <PopIn>
        <ConfirmButton_confirm onClick={() => handleClick(true)} />
      </PopIn>
    ) : (
      <PopIn>
        <ConfirmButton_confirmed onClick={() => handleClick(false)}/>
      </PopIn>
    )
  ) : (
    <Spinner />
  )
}

const formatPhone = phone => {
  return '0'+phone.substring(4)
}
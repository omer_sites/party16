// containers
import DelayRender from '../containers/DelayRender'

// images
import Tumbleweed from '../assets/tumbleweed.svg'

export default function Main_loggedIn_uninvited() {
  return (
  <DelayRender>
    <img src={Tumbleweed} style={{
      height: '10em',
      filter: 'grayscale(1) brightness(150%)',
    }}/>
    <h2
      style={{ color: '#666'}}
    >אינך קיים/ת ברשימת ההזמנה.</h2>
  </DelayRender>
  )
}
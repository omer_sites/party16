import { useState, useEffect } from 'react'

// custom hooks
import useFirebaseAuth from '../hooks/useFirebaseAuth'

// containers
import PopIn from '../containers/PopIn'

// components
import BigImgBtn from '../components/BigImgBtn'
import Spinner from '../components/Spinner'
import SingleInputForm from '../components/SingleInputForm'

// images
import LoginImg from '../assets/login.svg'

export default function Login() {

  const { UseRecaptcha, LoginWithPhone } = useFirebaseAuth()
  const [rendered, setRendered] = useState(false)
  const [verified, setVerified] = useState(false)
  const [sentSMS, setSentSMS] = useState(false)
  const [pending, setPending] = useState(false)

  useEffect(() => {
    UseRecaptcha('login-btn', onRecaptchaRender, onRecaptchaVerify)
  }, [])

  const onRecaptchaRender = () => setRendered(true)
  const onRecaptchaVerify = () => {
    setVerified(true) 
    setPending(false)
  }

  const handleClick = () => {
    setPending(true)
  }

  const handleSubmitPhone = phone => {
    setPending(true)
    LoginWithPhone(phone).then(() => {
      setPending(false)
      setSentSMS(true)
    }).catch(() => {
      setPending(false)
      alert('טלפון שגוי!')
    })
  }
  
  const handleSubmitCode = code => {
    setPending(true)
    window.confirmationResult.confirm(code).then(() => {
      // code success
    }).catch(() => {
      setPending(false)
      alert('קוד שגוי!')
    });
  }

  return (
    <>
    {!rendered && <Spinner />}
    {!verified ? (
      !pending ? (
        <BigImgBtn 
          onClick={handleClick}
          id="login-btn"
          className={`big-button ${!rendered ? 'hidden' : ''}`}
          img={LoginImg}
          tool-bottom="התחברות עם טלפון"
          style={{ '--color': '#b8e5ff' }}
          imgStyle={{ filter: 'invert(16%) sepia(59%) saturate(2959%) hue-rotate(182deg) brightness(93%) contrast(103%)' }}
        />
      ) : <Spinner />
    ) : (
      !pending ? (
        !sentSMS ? (
          <PopIn>
            <SingleInputForm onSubmit={handleSubmitPhone} placeholder="📞 מספר טלפון" />
          </PopIn>
        ) : (
          <PopIn>
            <SingleInputForm onSubmit={handleSubmitCode} placeholder="🔢 קוד אימות" />
          </PopIn>
        )
      ): <Spinner />
    )}
    </>
  )
}